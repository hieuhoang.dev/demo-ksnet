﻿using System.Data.SqlClient;
using System.Data;

namespace DemoLinqToSql
{
    class Program
    {
        static string connectionString = "Server=MSI;Database=FPS;Integrated Security = True;";
        
        static void Main(string[] args)
        {
            // Thêm một sản phẩm mới
            //AddProduct(1, "Laptop", 1200.00M);

            //// Truy vấn các sản phẩm
            var products = GetProducts();
            Console.WriteLine("Các sản phẩm trong cơ sở dữ liệu:");
            foreach (DataRow row in products.Rows)
            {
                Console.WriteLine($"ID: {row["Id"]}, Tên: {row["Name"]}, Giá: {row["NormalizedName"]}");
            }

            //// Cập nhật một sản phẩm
            //UpdateProduct(1, 1100.00M);

            //// Xóa một sản phẩm
            //DeleteProduct(1);
            //GetProducts();
        }

        static void AddProduct(int productId, string name, decimal price)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string query = "INSERT INTO Products (ProductID, Name, Price) VALUES (@ProductID, @Name, @Price)";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@Name", name);
                    command.Parameters.AddWithValue("@Price", price);
                    command.ExecuteNonQuery();
                }
            }
        }

        static DataTable GetProducts()
        {
            DataTable products = new DataTable();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string query = "SELECT * FROM Role";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(products);
                    }
                }
            }
            return products;
        }

        static void UpdateProduct(int productId, decimal newPrice)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string query = "UPDATE Products SET Price = @Price WHERE ProductID = @ProductID";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@Price", newPrice);
                    command.ExecuteNonQuery();
                }
            }
        }

        static void DeleteProduct(int productId)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string query = "DELETE FROM Products WHERE ProductID = @ProductID";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}