﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.DomainServices.Dtos
{
    public class UserDto
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
    }
}
