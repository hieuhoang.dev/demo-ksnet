﻿using KS.DomainServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.DomainServices.Services
{
    public interface IUserService
    {
        Task CreateUserAsync(UserDto userDto);
    }
}
