﻿using KS.DomainServices.Dtos;
using KS.Infrastructure.Entities;
using KS.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.DomainServices.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task CreateUserAsync(UserDto userDto)
        {
            var user = new User
            {
                FullName = userDto.FullName,
                UserName = userDto.UserName
            };

            await _userRepository.AddAsync(user);
        }
    }
}
