﻿using KS.Infrastructure.Entities;
using KS.Infrastructure.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.Infrastructure.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
