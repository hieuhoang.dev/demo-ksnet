﻿using KS.InfrastructureLayer.Configurations;
using KS.InfrastructureLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.InfrastructureLayer.Context
{
    public class WalletContext : DbContext
    {
        public WalletContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserConfiguration).Assembly);
        }

        public DbSet<User> Users { get; set; }  
        public DbSet<Role> Roles { get; set; }

    }
}
