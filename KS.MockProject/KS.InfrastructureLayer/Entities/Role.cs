﻿using KS.InfrastructureLayer.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.InfrastructureLayer.Entities
{
    public class Role : BaseEntity
    {
        public string RoleName { get; set; }
    }
}
