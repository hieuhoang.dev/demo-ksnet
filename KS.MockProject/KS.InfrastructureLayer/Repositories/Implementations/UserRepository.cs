﻿using KS.InfrastructureLayer.Context;
using KS.InfrastructureLayer.Entities;
using KS.InfrastructureLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.InfrastructureLayer.Repositories.Implementations
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(WalletContext context) : base(context)
        {
        }
    }
}
