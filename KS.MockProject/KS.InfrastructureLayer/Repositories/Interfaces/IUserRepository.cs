﻿using KS.InfrastructureLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.InfrastructureLayer.Repositories.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
    }
}
