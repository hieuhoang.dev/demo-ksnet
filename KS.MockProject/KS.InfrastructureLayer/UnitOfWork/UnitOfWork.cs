﻿using KS.InfrastructureLayer.Context;
using KS.InfrastructureLayer.Entities;
using KS.InfrastructureLayer.Repositories;
using KS.InfrastructureLayer.Repositories.Implementations;
using KS.InfrastructureLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.InfrastructureLayer.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WalletContext _context;

        public UnitOfWork(WalletContext context)
        {
            _context = context;
            Users = new UserRepository(_context);
            Roles = new RoleRepository(_context);
        }

        public IUserRepository Users { get; }
        public IRoleRepository Roles { get; }

        public async Task<int> SaveChangesAsync() => await _context.SaveChangesAsync();

        public void Dispose() => _context.Dispose();
    }
}
