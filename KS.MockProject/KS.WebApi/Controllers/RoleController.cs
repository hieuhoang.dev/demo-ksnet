﻿using AutoMapper;
using KS.Common.Response;
using KS.DomainLayer.Dtos;
using KS.DomainLayer.Services;
using KS.DomainLayer.Services.Interfaces;
using KS.InfrastructureLayer.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KS.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper;

        public RoleController(IRoleService roleService, IMapper mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllRoles()
        {
            var roles = await _roleService.GetAllRolesAsync();
            var roleDtos = _mapper.Map<IEnumerable<RoleDto>>(roles);
            return Ok(roleDtos);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetRoleById(int id)
        {
            var role = await _roleService.GetRoleByIdAsync(id);
            if (role == null)
                return NotFound();

            var roleDto = _mapper.Map<RoleDto>(role);
            return Ok(roleDto);
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(RoleDto roleDto)
        {
            var role = _mapper.Map<Role>(roleDto);
            await _roleService.AddRoleAsync(role);
            return CreatedAtAction(nameof(GetRoleById), new { id = role.Id }, roleDto);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateRole(int id, RoleDto roleDto)
        {
            if (id != roleDto.Id)
                return BadRequest();

            var role = _mapper.Map<Role>(roleDto);
            await _roleService.UpdateRoleAsync(role);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRole(int id)
        {
            await _roleService.DeleteRoleAsync(id);
            return NoContent();
        }

        [HttpGet("GetAllRoleWithPaging")]
        //[HttpGet]
        public TableResponse<RoleDto> GetAllRoleWithPaging(int start = 0, int length = 10, string search = "")
        {
            int totalRecords;
            var getAllRoleWithPaging = _roleService.GetAllRoleWithPaging(start, length, search, out totalRecords);
            var data = _mapper.Map<List<RoleDto>>(getAllRoleWithPaging);

            var response = new TableResponse<RoleDto>
            {
                Draw = 0,
                RecordsTotal = totalRecords,
                RecordsFiltered = totalRecords,
                Data = data
            };

            return response;
        }
    }
}
