﻿namespace KS.WebApi.Models
{
    public class CreateUserRequest
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
    }
}
