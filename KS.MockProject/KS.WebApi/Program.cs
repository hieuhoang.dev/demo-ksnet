using KS.DomainLayer.Services;
using KS.DomainLayer.Services.Interfaces;
using KS.InfrastructureLayer.Context;
using KS.InfrastructureLayer.UnitOfWork;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container
//builder.Services.AddTransient<IUserService, UserService>();
//builder.Services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
//builder.Services.AddTransient<IUserRepository, UserRepository>();

var connection = builder.Configuration.GetConnectionString("WalletConnection");

builder.Services.AddDbContext<WalletContext>(x => x.UseMySql(connection, ServerVersion.Parse("8.4.0-mysql")));

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddTransient<IUserService,UserService>();
builder.Services.AddTransient<IRoleService, RoleService>();

// Add AutoMapper
builder.Services.AddAutoMapper(typeof(KS.DomainLayer.Mappings.AutoMapperProfile));

// Add services to the container.   
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
