﻿using KS.Common.Response;
using KS.DomainLayer.Dtos;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace KS.WebApp.Controllers
{
    public class RoleController : Controller
    {
        [Route("danh-sach-quyen")]
        public async Task<ActionResult> Index()
        {
            // Gọi API để lấy dữ liệu
            var tableResponse = await GetAllRoleWithPaging();

            // Chuyển đổi dữ liệu sang ViewModel
            var viewModel = new TableResponse<RoleDto>
            {
                Code = tableResponse.Code,
                Success = tableResponse.Success,
                Message = tableResponse.Message,
                DataError = tableResponse.DataError,
                Draw = tableResponse.Draw,
                RecordsTotal = tableResponse.RecordsTotal,
                RecordsFiltered = tableResponse.RecordsFiltered,
                Data = new List<RoleDto>()
            };

            foreach (var item in tableResponse.Data)
            {
                viewModel.Data.Add(new RoleDto
                {
                    Id = item.Id,
                    RoleName = item.RoleName,
                    // Ánh xạ các thuộc tính khác tương ứng
                });
            }

            return View(viewModel);
        }

        public async Task<TableResponse<RoleDto>> GetAllRoleWithPaging()
        {
            TableResponse<RoleDto> tableResponse = new TableResponse<RoleDto>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:7065/api/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Role/GetAllRoleWithPaging");
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    tableResponse = JsonConvert.DeserializeObject<TableResponse<RoleDto>>(jsonResponse);
                }
                else
                {
                    // Xử lý khi gọi API không thành công
                    // Ví dụ: throw new Exception("Failed to retrieve data from API");
                    tableResponse.Code = (int)response.StatusCode;
                    tableResponse.Success = false;
                    tableResponse.Message = "Failed to retrieve data from API";
                }
            }

            return tableResponse;
        }

    }
}
