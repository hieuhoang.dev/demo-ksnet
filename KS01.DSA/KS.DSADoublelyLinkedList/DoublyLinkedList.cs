﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.DSADoublelyLinkedList
{
    internal class DoublyLinkedList
    {
        Node head = null;

        public void append(int data)
        {
            Node newNode = new Node(data);
            if (head == null)
            {
                head = newNode;
                return;
            }

            Node current = head;
            while (current.next != null)
            {
                current = current.next;
            }
            current.next = newNode;
            newNode.prev = current;
        }

        public void display()
        {
            Node current = head;
            while (current != null)
            {
                Console.Write(current.data + " <-> ");
                current = current.next;
            }
            Console.WriteLine("null");
        }

        public void InsertAtPosition(int data, int position)
        {
            Node newNode = new Node(data);
            if (position == 0)
            {
                newNode.next = head;
                head.prev = newNode;
                head = newNode;
                return;
            }
            Node current = head;
            for (int i = 0; i < position - 1; i++)
            {
                current = current.next;
            }
            newNode.next = current.next;
            current.next = newNode;
            newNode.prev = current;
            newNode.next.prev = newNode;
        }

        public void AppendAtOrder(int order, int data)
        {
            //int length = GetLength();
            Node newNode = new Node(data);
            if (head == null)
            {
                head = newNode;
            }
            Node currentNode = head;
            int currentIndex = 1;
            while (currentNode.next != null && currentIndex < order)
            {
                currentNode = currentNode.next;
                currentIndex++;
            }
            if (currentNode.next == null)
            {
                currentNode.next = newNode;
            }
            else
            {
                newNode.next = currentNode.next;
                currentNode.next.prev = newNode;
                newNode.prev = currentNode;
                currentNode.next = newNode;
            }

        }


    }
}
