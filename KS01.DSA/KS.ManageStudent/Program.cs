﻿
using KS.ManageStudent;

while (true)
{
    Console.WriteLine("Student Management System");
    Console.WriteLine("1. Add new student to list of student");
    Console.WriteLine("2. Show list students");
    Console.WriteLine("3. Show list students ordered by GPA ascending");
    Console.WriteLine("4. Show students with highest GPA of each Grade and ordered by GPA desending");
    Console.WriteLine("5. Exit program");
    Console.WriteLine("Your choice: ");

    int choice = int.Parse(Console.ReadLine());

    switch (choice)
    {
        case 1:
            StudentManagement.inputStudent();
            break;
        case 2:
            StudentManagement.displayStudent();
            break;
        case 3:
            StudentManagement.displayStudentWithGpaAsc();
            break;
        case 4:
            break;
        case 5:
            Console.WriteLine("Application end!");
            Environment.Exit(0);
            break;
    }

}
