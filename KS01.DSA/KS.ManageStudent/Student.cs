﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.ManageStudent
{
    internal class Student
    {
        public int code;
        public string name;
        public int age;
        public int grade;
        public double gpa;
        public Student next;

        public Student(int code, string name, int age, int grade, double gpa)
        {
            this.code = code;
            this.name = name;
            this.age = age;
            this.grade = grade;
            this.gpa = gpa;
            this.next = null;
        }
    }
}
