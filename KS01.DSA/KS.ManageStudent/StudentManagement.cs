﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.ManageStudent
{
    internal class StudentManagement
    {
        static Student head = null;

        public static bool CheckCodeUnique(int code)
        {
            bool equal = false;

            Student current = head;
            while (current != null)
            {
                if (current.code == code)
                {
                    equal = true;
                }
                current = current.next;
            }

            return equal;
        }

        public static void inputStudent()
        {
            Console.WriteLine("Input infomation student: ");
            Console.WriteLine("Code student (> 0): ");
            int code = int.Parse(Console.ReadLine());
            bool checkCode = CheckCodeUnique(code);
            while (code < 0 || checkCode == true)
            {
                Console.WriteLine("Code student again: ");
                code = int.Parse(Console.ReadLine());
                checkCode = CheckCodeUnique(code);
            }
            Console.WriteLine("Name student: ");
            string name = Console.ReadLine();
            Console.WriteLine("Age student (> 7): ");
            int age = int.Parse(Console.ReadLine());
            while (age < 7)
            {
                Console.WriteLine("Age student again: ");
                age = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Grade student (>= 1 & <= 5): ");
            int grade = int.Parse(Console.ReadLine());
            while (grade < 1 || grade > 5)
            {
                Console.WriteLine("Grade student again: ");
                grade = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("GPA student (>= 0.0 & <= 10.0): ");
            double gpa = double.Parse(Console.ReadLine());
            while (gpa < 0.0 || gpa > 10.0)
            {
                Console.WriteLine("GPA student again: ");
                gpa = double.Parse(Console.ReadLine());
            }

            Student newNode = new Student(code, name, age, grade, gpa);

            if (head == null)
            {
                head = newNode;
                return;
            }

            Student current = head;
            while (current.next != null)
            {
                current = current.next;
            }

            current.next = newNode;
        }

        public static void displayStudent()
        {
            if (head == null)
            {
                Console.WriteLine("List Student is emty!");
            }
            else
            {
                Student current = head;
                while (current != null)
                {
                    Console.WriteLine(current.code + " - "
                                    + current.name + " - "
                                    + current.age + " - "
                                    + current.grade + " - "
                                    + current.gpa);
                    current = current.next;
                }
            }
        }

        public static void displayStudentWithGpaAsc()
        {
            //c1
            //b1 tạo một linked list mới
            //b2 tìm kiếm student có gpa nhỏ nhất
            //b3 dùng vòng lặp while để thêm vào linkedlist mới
            //và mỗi lần thêm là xóa node của linked list cũ
            //kết quả sẽ được một linked list mới được sắp xếp
            //và linked list cũ sẽ null

            //c2
            //sẽ tạo một node swap
            //tìm kiếm student có gpa nhỏ nhất 
            //sau đó nạp thông tin vào cái node
            //xóa node nhỏ nhất rồi đẩy cáo swap lên head

            Student current = head;
            while (current != null)
            {
                Student min = head;
                Student swapNode = head;
                while (swapNode != null)
                {
                    if (swapNode.gpa < min.gpa)
                    {
                        min = swapNode;
                    }
                    swapNode = swapNode.next;
                }

                int code = current.code;
                string name = current.name;
                int age = current.age;
                int grade = current.grade;
                double gpa = current.gpa;

                current.code = min.code;
                current.name = min.name;
                current.age = min.age;
                current.gpa = min.gpa;
                current.grade = min.grade;

                min.code = code;
                min.name = name;
                min.age = age;
                min.grade = grade;
                min.gpa = gpa;

                current = current.next;

                displayStudent();
            }


            //thêm một biến index để lấy được cái vị trí của node mà đang là nhỏ nhất
            //thêm swap vào head và remove node ở vị trí vừa tìm được

            //10 - 1 - 2
            //1
            //10 - 2
            //1 - 10 - 2

            //tạo một linked list mới
            // mỗi một lần tìm được node nhỏ nhất
            //add và cuối của linked list mới
            //remove ở linked list cũ

            //Nổi bọt
            //không thay đổi các vị trí của node
            //chỉ thay đổi giá tri
            //nghĩa là sẽ thành tìm giá trị lớn nhất nạp vào cuối

        }

        public void dispplayTopGrade()
        {
            Student gradeOne = null;
            Student gradeTwo = null;
            Student gradeThree = null;
            Student gradeFour = null;
            Student gradeFive = null;

            Student current = head;
            while(current != null)
            {
                if (current.grade == 1)
                {
                    if(gradeOne.gpa < current.gpa)
                    {
                        gradeOne = current;
                    }
                }

                current = current.next;
            }
        }
    }
}
