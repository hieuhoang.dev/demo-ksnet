﻿// See https://aka.ms/new-console-template for more information


using KS01.DSA;

SinglyLinkedList singlyLinkedList = new SinglyLinkedList();
singlyLinkedList.display();

singlyLinkedList.append(1);
singlyLinkedList.append(2);
singlyLinkedList.append(3);
singlyLinkedList.append(4);
singlyLinkedList.append(5);
singlyLinkedList.append(6);
singlyLinkedList.appendMiddle(0);
singlyLinkedList.display();
singlyLinkedList.deleteWithIndex(4);
singlyLinkedList.display();
