﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace KS01.DSA
{
    public class SinglyLinkedList
    {
        Node head = null;

        public void append(int data)
        {
            Node newNode = new Node(data);

            if (head == null)
            {
                head = newNode;
                return;
            }

            Node current = head;
            while (current.next != null)
            {
                current = current.next;
            }
            current.next = newNode;
        }

        public void display()
        {
            Node current = head;
            while (current != null)
            {
                Console.Write(current.data + " -> ");
                current = current.next;
            }
            Console.WriteLine("null");
        }

        public void appendMiddle(int data)
        {
            Node newNode = new Node(data);

            int count = 1;
            Node current = head;

            while (current.next != null)
            {
                count++;
                current = current.next;
            }

            //int middle = (count % 2 == 0) ? (count / 2) : (count / 2 + 1);
            int middle = 0;
            if (count % 2 == 0)
            {
                middle = count / 2;

            }
            else
            {
                middle = count / 2 + 1;
            }
            count = 1;
            current = head;
            while (current.next != null)
            {
                count++;
                current = current.next;
                if (count == middle)
                {
                    break;
                }

            }

            newNode.next = current.next;
            current.next = newNode;
        }

        public void deleteWithIndex(int index)
        {
            int count = 1;
            Node current = head;

            while (current.next != null)
            {
                count++;
                current = current.next;
            }

            count = 1;
            current = head;
            while (current.next != null)
            {
                count++;
                current = current.next;
                if (count == index - 1)
                {
                    break;
                }
            }

            current.next = current.next.next;
        }

    }
}
