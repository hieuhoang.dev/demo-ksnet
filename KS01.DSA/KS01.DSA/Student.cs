﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS01.DSA
{
    internal class Student
    {
        public int id;
        public string name;
        public int age;
        public Student next;

        public Student(int id, string name, int age)
        {
            this.id = id;
            this.name = name;
            this.age = age;
            this.next = null;
        }
    }
}
