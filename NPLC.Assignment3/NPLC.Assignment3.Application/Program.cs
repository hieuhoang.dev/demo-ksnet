﻿// See https://aka.ms/new-console-template for more informatio


//Appilcation gọi đến Service thì nghĩa là 
//Application có thể sử dụng được các class trong service
//Nhưng không thể làm ngược lại

//Tương tự
//Entity đang gọi đến Application nên 
//Entity sẽ sử dụng được các class trong Application 
//và không thể ngược lại

using NPLC.Assignment3.Service;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

while (true)
{
    Console.WriteLine("Hệ thống quản lý nhân sự");
    Console.WriteLine("1.Nhập thông tin phòng ban");
    Console.WriteLine("2.Hiển thị danh sách nhân viên");
    Console.WriteLine("3.Phân loại nhân viên");
    Console.WriteLine("4.Tìm kiếm nhân viên");
    Console.WriteLine("5.Báo cáo thống kê");

    Console.WriteLine("Mời bạn nhập lựa chọn: ");
    int choice = Int32.Parse(Console.ReadLine());

    switch (choice)
    {
        case 1:
            EmployeeManagement.InputData();
            break;
        case 2:
            EmployeeManagement.DisplayEmployees();
            break;
        case 3:
            EmployeeManagement.ClassifyEmployees();
            break;
        case 4:
            EmployeeManagement.EmployeeSearch();
            break;
        case 5:
            EmployeeManagement.Report();
            break;
        default:
            break;
    }
}

