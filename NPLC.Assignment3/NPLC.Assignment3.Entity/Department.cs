﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPLC.Assignment3.Entity
{
    public class Department
    {
        public string DepartmentName { get; set; }
        public List<Employee> employees { get; set; }
        
        public Department() { } 

        public Department(string departmentName)
        {
            DepartmentName = departmentName;
            employees = new List<Employee>();
        }

        public void AddEmpoyee(Employee employee)
        {
            employees.Add(employee);
        }

        public int CountOf<T>() where T : Employee
        {
            return employees.OfType<T>().Count();
        }

        //Phân biệt List và ArrayList
        public List<T> GetEmployees<T>() where T : Employee
        {
            return employees.OfType<T>().ToList();
        }

        public override string ToString()
        {
            return $"Department: {DepartmentName}, CountOfEmployee: {employees.Count} ";
        }

        public void DisplayEmployee()
        {
            foreach (var item in employees)
            {
                Console.WriteLine(item);
            }
        }

        public List<Employee> GetAllEmployees()
        {
            return employees;
        }
    }
}
