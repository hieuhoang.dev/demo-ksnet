﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPLC.Assignment3.Entity
{
    public abstract class Employee
    {
        //phân biệt field và property
        public string Ssn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        private DateTime birthDate;

        public DateTime BirthDate
        {
            get { return birthDate; }
            set
            {
                if (value >= DateTime.Now)
                {
                    throw new ArgumentException("Ngày sinh phải nhỏ hơn ngày hiện tại");
                }
                birthDate = value;
            }
        }

        private string phone;

        public string Phone
        {
            get { return phone; }
            set
            {
                if (!Regex.IsMatch(value, @"^\d{7,}$"))
                {
                    throw new ArgumentException("Số điện thoại phải từ 7 ký tự");
                }
                phone = value;
            }
        }
        public string Email { get; set; }

        public Employee()
        {

        }

        public Employee(string ssn, string firstName, string lastName)
        {
            Ssn = ssn;
            FirstName = firstName;
            LastName = lastName;
        }

        public Employee(string ssn, string firstName, string lastName, DateTime birthDate, string phone, string email)
        {
            Ssn = ssn;
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
            Phone = phone;
            Email = email;
        }

        public override string ToString()
        {
            return $"Ssn : {Ssn}, Name : {FirstName} {LastName}, Birth Date: {BirthDate.ToString("dd/MM/yyyy")}, Phone : {Phone}, Email : {Email}";
        }
    }
}
