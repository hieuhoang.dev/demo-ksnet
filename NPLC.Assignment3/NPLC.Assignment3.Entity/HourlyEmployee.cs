﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace NPLC.Assignment3.Entity
{
    public class HourlyEmployee : Employee
    {
        public double Rate { get; set; }
        public double WorkingHours { get; set; }

        public HourlyEmployee() : base()
        {
            
        }

        public HourlyEmployee(double rate, double workingHours, string ssn, string firstName, string lastName) : base(ssn, firstName, lastName)
        {
            Rate = rate;
            WorkingHours = workingHours;
        }

        public HourlyEmployee(double rate, double workingHours, string ssn, string firstName, string lastName, DateTime birthDate, string phone, string email) : base(ssn, firstName, lastName, birthDate, phone, email)
        {
            Rate = rate;
            WorkingHours = workingHours;
        }

        public override string ToString()
        {
            return base.ToString() + $"Rate : {Rate}, WorkingHours: {WorkingHours}";
        }
    }
}
