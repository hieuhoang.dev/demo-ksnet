﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPLC.Assignment3.Entity
{
    public class SalaryEmployee : Employee
    {
        //CommisstionRate, GrossSales, BasicSalary
        public double CommisstionRate { get; set; }
        public double GrossSale { get; set; }
        public double BasicSalary { get; set; }

        public SalaryEmployee() : base()
        {
            
        }

        public SalaryEmployee(double commisstionRate, double grossSale, double basicSalary, string ssn, string firstName, string lastName) : base(ssn, firstName, lastName) 
        {
            CommisstionRate = commisstionRate;
            GrossSale = grossSale;
            BasicSalary = basicSalary;
        }

        public SalaryEmployee(double commisstionRate, double grossSale, double basicSalary, string ssn, string firstName, string lastName, DateTime birthDate, string phone, string email) : base(ssn, firstName, lastName, birthDate, phone, email)
        {
            CommisstionRate = commisstionRate;
            GrossSale = grossSale;
            BasicSalary = basicSalary;
        }

        public override string ToString()
        {
            return base.ToString() + $"CommisstionRate : {CommisstionRate}, GrossSale : {GrossSale}, BasicSalary : {BasicSalary}";
        }
    }
}
