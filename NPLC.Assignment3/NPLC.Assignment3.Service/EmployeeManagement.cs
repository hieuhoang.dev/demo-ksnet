﻿using NPLC.Assignment3.Entity;
using NPLC.Assignment3.Service.Validator;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPLC.Assignment3.Service
{
    public class EmployeeManagement
    {
        public static List<SalaryEmployee> salaryEmployees = new List<SalaryEmployee>();
        public static List<HourlyEmployee> hourlyEmployees = new List<HourlyEmployee>();
        public static List<Department> departments = new List<Department>();

        //public static void InputEmployee()
        //{
        //    Console.WriteLine("Mời bạn nhập loại nhân viên muốn thêm");
        //    Console.WriteLine("1.Nhân viên chính thức");
        //    Console.WriteLine("2.Nhân viên làm theo giờ ");

        //    int choice = Convert.ToInt32(Console.ReadLine());
        //    //while (choice != 1 && choice != 2)
        //    //{
        //    //    Console.WriteLine("Mời bạn nhập lại lựa chọn: ");
        //    //    choice = Convert.ToInt32(Console.ReadLine());
        //    switch (choice)
        //    {
        //        case 1:
        //            SalaryEmployee salaryEmployee = new SalaryEmployee();
        //            Console.WriteLine("Mời bạn nhập mã nhân viên: ");
        //            salaryEmployee.Ssn = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập tên nhân viên: ");
        //            salaryEmployee.FirstName = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập họ và tên đệm nhân viên: ");
        //            salaryEmployee.LastName = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập ngày sinh nhân viên: ");
        //            salaryEmployee.BirthDate = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập số điện thoại nhân viên: ");
        //            salaryEmployee.Phone = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập email nhân viên: ");
        //            string email = Console.ReadLine();
        //            bool checkEmail = ValidateEmail.CheckEmail(email);
        //            while (checkEmail == false)
        //            {
        //                checkEmail = ValidateEmail.CheckEmail(email);
        //                Console.WriteLine("Mời bạn nhập lại email nhân viên: ");
        //                email = Console.ReadLine();
        //            }
        //            salaryEmployee.Email = email;
        //            Console.WriteLine("Nhập tỷ lệ hoa hồng: ");
        //            salaryEmployee.CommisstionRate = Convert.ToDouble(Console.ReadLine());
        //            Console.WriteLine("Nhập lương bán hàng: ");
        //            salaryEmployee.GrossSale = Convert.ToDouble(Console.ReadLine());
        //            Console.WriteLine("Nhập lương cơ bản: ");
        //            salaryEmployee.BasicSalary = Convert.ToDouble(Console.ReadLine());
        //            salaryEmployees.Add(salaryEmployee);
        //            break;
        //        case 2:
        //            HourlyEmployee hourlyEmployee = new HourlyEmployee();
        //            Console.WriteLine("Mời bạn nhập mã nhân viên: ");
        //            hourlyEmployee.Ssn = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập tên nhân viên: ");
        //            hourlyEmployee.FirstName = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập họ và tên đệm nhân viên: ");
        //            hourlyEmployee.LastName = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập ngày sinh nhân viên: ");
        //            hourlyEmployee.BirthDate = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập số điện thoại nhân viên: ");
        //            hourlyEmployee.Phone = Console.ReadLine();
        //            Console.WriteLine("Mời bạn nhập email nhân viên: ");
        //            hourlyEmployee.Email = Console.ReadLine();
        //            Console.WriteLine("Nhập lương 1 giờ: ");
        //            hourlyEmployee.Rate = Convert.ToDouble(Console.ReadLine());
        //            Console.WriteLine("Nhập giờ làm thực tế: ");
        //            hourlyEmployee.WorkingHours = Convert.ToDouble(Console.ReadLine());
        //            hourlyEmployees.Add(hourlyEmployee);
        //            break;
        //        default:
        //            Console.WriteLine("Nhập sai rồi");
        //            break;
        //            //}
        //    }
        //}

        //public static void DisplayEmployee()
        //{
        //    Console.WriteLine("Nhân viên chính thức: ");
        //    foreach (var item in salaryEmployees)
        //    {
        //        Console.WriteLine(item.Ssn + " - "
        //                        + item.FirstName + " - "
        //                        + item.LastName + " - "
        //                        + item.BirthDate + " - "
        //                        + item.Phone + " - "
        //                        + item.Email + " - "
        //                        + item.CommisstionRate + " - "
        //                        + item.GrossSale + " - "
        //                        + item.BasicSalary);
        //    }

        //    Console.WriteLine("Nhân viên làm theo giờ: ");
        //    foreach (var item in hourlyEmployees)
        //    {
        //        Console.WriteLine(item.Ssn + " - "
        //                        + item.FirstName + " - "
        //                        + item.LastName + " - "
        //                        + item.BirthDate + " - "
        //                        + item.Phone + " - "
        //                        + item.Email + " - "
        //                        + item.Rate + " - "
        //                        + item.WorkingHours);
        //    }
        //}

        public static void InputData()
        {
            Console.WriteLine("Nhập tên phòng ban: ");
            string departmentName = Console.ReadLine();
            Department department = departments.Find(d => d.DepartmentName == departmentName);
            if (department == null)
            {
                department = new Department(departmentName);
                departments.Add(department);
            }
            Console.WriteLine("Mời bạn nhập loại nhân viên muốn thêm");
            Console.WriteLine("1.Nhân viên chính thức");
            Console.WriteLine("2.Nhân viên làm theo giờ ");
            int choice = Convert.ToInt32(Console.ReadLine());
            while (choice != 1 && choice != 2)
            {
                Console.WriteLine("Mời bạn nhập lại loại nhân viên muốn thêm");
                Console.WriteLine("1.Nhân viên chính thức");
                Console.WriteLine("2.Nhân viên làm theo giờ ");
                choice = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Mời bạn nhập mã nhân viên: ");
            string ssn = Console.ReadLine();
            Console.WriteLine("Mời bạn nhập tên nhân viên: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Mời bạn nhập họ và tên đệm nhân viên: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Mời bạn nhập ngày sinh nhân viên: ");
            DateTime birthDate = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            Console.WriteLine("Mời bạn nhập số điện thoại nhân viên: ");
            string phone = Console.ReadLine();
            Console.WriteLine("Mời bạn nhập email nhân viên: ");
            string email = Console.ReadLine();

            if (choice == 1)
            {
                Console.WriteLine("Nhập tỷ lệ hoa hồng: ");
                double commisstionRate = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Nhập lương bán hàng: ");
                double grossSale = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Nhập lương cơ bản: ");
                double basicSalary = Convert.ToDouble(Console.ReadLine());
                SalaryEmployee salaryEmployee = new SalaryEmployee(commisstionRate,
                                                                   grossSale,
                                                                   basicSalary,
                                                                   ssn,
                                                                   firstName,
                                                                   lastName,
                                                                   birthDate,
                                                                   phone,
                                                                   email);
                department.AddEmpoyee(salaryEmployee);
            }
            else if (choice == 2)
            {
                Console.WriteLine("Nhập lương 1 giờ: ");
                double rate = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Nhập giờ làm thực tế: ");
                double workingHours = Convert.ToDouble(Console.ReadLine());
                HourlyEmployee hourlyEmployee = new HourlyEmployee(rate,
                                                                   workingHours,
                                                                   ssn,
                                                                   firstName,
                                                                   lastName,
                                                                   birthDate,
                                                                   phone,
                                                                   email);
                department.AddEmpoyee(hourlyEmployee);
            }
            else
            {
                Console.WriteLine("Nhập sai loại nhân viên");
            }
        }

        public static void DisplayEmployees()
        {
            foreach (var item in departments)
            {
                Console.WriteLine(item);
                item.DisplayEmployee();
            }
        }

        public static void ClassifyEmployees()
        {
            foreach (var item in departments)
            {
                foreach (var emp in item.GetAllEmployees())
                {
                    if(emp is SalaryEmployee)
                    {
                        Console.WriteLine("Nhân viên chính thức: " + emp);
                    }
                    else if(emp is HourlyEmployee)
                    {
                        Console.WriteLine("Nhân viên làm theo giờ: " + emp);
                    }
                }
            }
        }

        public static void EmployeeSearch()
        {
            Console.WriteLine("1.Tìm kiếm nhân viên theo phòng ban");
            Console.WriteLine("2.Tìm kiếm nhân viên theo tên ");
            int choice = Convert.ToInt32(Console.ReadLine());
            while (choice != 1 && choice != 2)
            {
                Console.WriteLine("Mời bạn nhập lại lựa chọn tìm kiếm");
                Console.WriteLine("1.Tìm kiếm nhân viên theo phòng ban");
                Console.WriteLine("2.Tìm kiếm nhân viên theo tên ");
                choice = Convert.ToInt32(Console.ReadLine());
            }

            if (choice == 1)
            {
                Console.WriteLine("Nhập tên phòng ban: ");
                string departmentName = Console.ReadLine();
                Department department = departments.Find(d => d.DepartmentName == departmentName);

                if (department != null)
                {
                    Console.WriteLine("1.Nhân viên chính thức");
                    Console.WriteLine("2.Nhân viên làm theo giờ ");
                    int choiceTypeEmployee = Convert.ToInt32(Console.ReadLine());
                    while (choiceTypeEmployee != 1 && choiceTypeEmployee != 2)
                    {
                        Console.WriteLine("Mời bạn nhập lại lựa chọn tìm kiếm");
                        Console.WriteLine("1.Nhân viên chính thức");
                        Console.WriteLine("2.Nhân viên làm theo giờ ");
                        choiceTypeEmployee = Convert.ToInt32(Console.ReadLine());
                    }

                    if (choiceTypeEmployee == 1)
                    {
                        List<SalaryEmployee> salaryEmployees = department.GetEmployees<SalaryEmployee>();

                        foreach (var item in salaryEmployees)
                        {
                            Console.WriteLine(item);
                        }
                    }
                    else if (choiceTypeEmployee == 2)
                    {
                        List<HourlyEmployee> hourlyEmployees = department.GetEmployees<HourlyEmployee>();

                        foreach (var item in hourlyEmployees)
                        {
                            Console.WriteLine(item);
                        }
                    }
                }
            }
            else if (choice == 2)
            {
                Console.WriteLine("Nhập tên cần tìm kiếm: ");
                string empName = Console.ReadLine();
                foreach(var department in departments)
                {
                    foreach(var emp in department.GetAllEmployees())
                    {
                        if ((emp.FirstName + " " + emp.LastName).ToLower().Contains(empName.ToLower()))
                        {
                            Console.WriteLine(emp);
                        }
                    }
                }
            }
        }

        public static void Report()
        {
            foreach (var item in departments)
            {
                Console.WriteLine($"{item.DepartmentName} có {item.GetAllEmployees().Count} nhân viên");
            }
        }
    }
}
